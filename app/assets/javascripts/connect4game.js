var gameMatrix;
var initialized = false;
var player = "red";
var discsLeft = 42;
var gameHasEnded = false;

function initialize(){
    gameMatrix = [
        {used: 0, tower: [0, 0, 0, 0, 0, 0 ]}, 
        {used: 0, tower: [0, 0, 0, 0, 0, 0 ]}, 
        {used: 0, tower: [0, 0, 0, 0, 0, 0 ]}, 
        {used: 0, tower: [0, 0, 0, 0, 0, 0 ]}, 
        {used: 0, tower: [0, 0, 0, 0, 0, 0 ]}, 
        {used: 0, tower: [0, 0, 0, 0, 0, 0 ]}, 
        {used: 0, tower: [0, 0, 0, 0, 0, 0 ]}, 
    ]

    initialized = true;
}

function dropDisc(tableColumn){
    if (!initialized) {
        initialize()
        console.log("Initialized");    
    }
    
    if (! gameHasEnded){
        if (gameMatrix[tableColumn].used < 6){
            gameMatrix[tableColumn].tower[gameMatrix[tableColumn].used] = player;
            document.getElementById(tableColumn + ""+gameMatrix[tableColumn].used).style.backgroundColor = player

            discsLeft -= 1;

            if (discsLeft <= 35){
                if (gameIsWon(tableColumn, gameMatrix[tableColumn].used, player)){
                    document.getElementById("announce").innerHTML = player + " Won!"
                    return
                }
            }

            gameMatrix[tableColumn].used += 1;
            player = (player == "red") ? "blue" : "red";

            document.getElementById("announce").innerHTML = player + " playing"
            
        }
    }

    if (discsLeft <= 0){
      document.getElementById("announce").innerHTML = "Game Over. No more Discs"
    }
}

function gameIsWon (arrayIndex, towerIndex, player){
    console.log("checking for winner. last disc placed on " + arrayIndex + "," + towerIndex );
    
    //check vertical
    if (is4connected(gameMatrix[arrayIndex].tower.join(''))){
        console.log(player + " WON!!!! vertically");
        return true;
    }

    //check horizontal
    var horizontalString = '';
    for (indx = 0; indx <=6; indx++){
        horizontalString = horizontalString.concat('', gameMatrix[indx].tower[towerIndex]); 
    }
    if (is4connected(horizontalString)){
        console.log(player + " WON!!!! horizontaly");
        return true;
    }

    //check diagonal
    var diagonalString = '';
    var indxIncrementer = 0;
    var startx = arrayIndex - towerIndex;
    var starty = 0;
    if ( startx < 0){
        startx = 0;
        starty = towerIndex - arrayIndex;
    }

    do{
        diagonalString = diagonalString.concat(gameMatrix[startx + indxIncrementer].tower[starty + indxIncrementer]);
        indxIncrementer +=1;
    }while(startx + indxIncrementer <= 6 && starty + indxIncrementer <= 5);

    if (is4connected(diagonalString)){
        console.log(player + " WON!!!! diag bottom to right");
        return true;
    }

    diagonalString = '';
    indxIncrementer = 0;

    startx = arrayIndex + towerIndex;
    starty = 0;
    if ( startx > 6){
        starty = startx - 6;
        startx = 6;
    }

    do{ 
        diagonalString = diagonalString.concat(gameMatrix[startx - indxIncrementer].tower[starty+indxIncrementer]);
        indxIncrementer +=1;
    }while(starty + indxIncrementer < 6 && startx - indxIncrementer >= 0);

    if (is4connected(diagonalString)){
        console.log(player + " WON!!!! diag bottom to left");
        return true;
    }

}

function is4connected(stringToSearch){
    var regexStr = new RegExp ( "(" + player + "){4}")
    if (stringToSearch.match(regexStr)){
        gameHasEnded = true
        return true;
    }else {
        return false;
    }
}