Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'static_pages/connect4game'

  root 'static_pages#connect4game'
  
end
